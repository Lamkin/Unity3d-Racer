﻿using UnityEngine;
using System.Collections.Generic;

public class CameraSwitch : MonoBehaviour
{
   public List<Camera> m_cameras;
    //public Camera FPVCam;
    //public Camera TPVCam;
    //public Camera RVCam;
    private int m_camIndex = 0;

    void Start()
    {
        disableAllCameras();
        m_cameras[0].enabled = true;
    }

    void disableAllCameras()
    {
        foreach (Camera c in m_cameras)
        {
            c.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("Fire1"))
        {
            m_camIndex = (m_camIndex + 1) % m_cameras.Count;
            disableAllCameras();
            m_cameras[m_camIndex].enabled = true;
        }
    }
}
